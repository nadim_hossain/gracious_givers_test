<?php
	error_reporting(0);
	$hostname = "localhost";
	$username = "root";
	$password = "";
	$database = "ngo_database";

	$conn = new mysqli($hostname, $username, $password, $database);

	if ($conn->connect_error) {
		die("Failed to Connect: " . $conn->connect_error);
	}

		if($_SERVER['REQUEST_METHOD'] === "POST"){
			$username = $_POST['username'];
			$password = $_POST['password'];
			$rememberme = isset($_POST['rememberme']) ? $_POST['rememberme'] : null ;
			
			if(empty($username) || empty($password)){
				echo "Please Enter Username Or Password";
			}
			else{

				$sql = "SELECT User_Name, Ngo_Password FROM ngo_data where User_Name = ? and Ngo_Password = ?";

				$stmt = $conn->prepare($sql);
				$stmt->bind_param('ss', $User_Name, $Ngo_Password);
				$User_Name = $username;
				$Ngo_Password = $password;

				$stmt->execute();
				$result = $stmt->get_result();

				if ($result->num_rows > 0) {

					while($row = $result->fetch_assoc()) {
						$login = true;
					}
				}
					if($login){
						if($rememberme === "on"){
							setcookie("username", $username, time() + 3600);
						}
						session_start();
						$_SESSION['username'] = $username;
						header("Location: ../View/NGO_Welcome_Page.php");
					}
					else{
						echo "Login Failed....";
					}
			}
		}
	$conn->close();
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>NGO'S Login Page</title>
</head>
<body>
	<center> <h1>Welcome To NGO'S Login Form</h1> </center>
	<br>
	<br>
	<br>
	<br><br><br>
		<form action="" method="POST">
			<fieldset>
                <legend>Login Form :</legend>
                <br>
				<center> Username : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="textarea" name="username" placeholder="Username" value="<?php echo isset($_COOKIE['username']) ? $_COOKIE['username'] : "" ?>" </center>
                <br>
				<br>
                <center> Password : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="password" name ="password" placeholder="Password" </center>
				<center> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="../View/Username_Verification.php"> Forget Password</a> </center>
				<br>
				<center> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="rememberme">Remember Me</center>
				<br>
            </fieldset>
			<br>
			<center> <input type="submit" value="Login" style="width: 15%;" > </center>
			<br>
			<center> Don't have a account yet? <a href="NGO_Registrasion_Page.php"> Created a account</a> </center>
		</form>
		<br><br><br><br><br><br><br><br><br><br><br><br>
		<hr>
		<br>
		<?php
		
			include '../Controller/Footer.php';
		?>
</body>
</html>