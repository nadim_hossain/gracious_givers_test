<?php

	$confirmpassword = "";
	$password = "";
	$confirmpasswordErrMsg = '';
	$passwordErrMsg = '';

	if ($_SERVER['REQUEST_METHOD'] === "POST"){
			$number = preg_match('@[0-9]@', $_POST['password']);
			$uppercase = preg_match('@[A-Z]@', $_POST['password']);
			$lowercase = preg_match('@[a-z]@', $_POST['password']);
			$specialChars = preg_match('@[^\w]@', $_POST['password']);
			
			if(empty($_POST['password'])){
				$passwordErrMsg = "Enter Password.";
			}
			else if(strlen( $_POST['password']) < 8 || !$number || !$uppercase || !$lowercase || !$specialChars) {
				$passwordErrMsg = "Password must be at least 8 characters in length and must contain at least one number,
									one upper case letter, one lower case letter and one special character.";
			}
			else{
				$password = $_POST['password'];
			}
	
			if(empty($_POST['confirmpassword'])){
				$confirmpasswordErrMsg = "*Enter Confirm Password";
			}
			else if($_POST['password'] != $_POST['confirmpassword']){
				$confirmpasswordErrMsg = "Enter Same Password.";
			}
			else{
				$confirmpassword = $_POST['confirmpassword'];
			}
			
			if($confirmpasswordErrMsg === '' && $passwordErrMsg === ''){
				session_start();
				$_SESSION['username'];
				$hostname = "localhost";
				$username = "root";
				$password = "";
				$database = "ngo_database";

				$conn = new mysqli($hostname, $username, $password, $database);

				if ($conn->connect_error) {
					die("Failed to Connect: " . $conn->connect_error);
				}

				$sql = "UPDATE ngo_data SET Ngo_Password = ? WHERE User_Name = ?";

				$stmt = $conn->prepare($sql);
				$stmt->bind_param('ss', $Ngo_Password, $User_Name);
				$Ngo_Password = $confirmpassword;
				$User_Name = $_SESSION['username'];
				if ($stmt->execute()) {
					echo "Data update successful";
					echo "<hr>";
				}
				else {
					echo "Failed to update data";
					echo "<hr>";
				}
				$conn->close();
			}
			else{
				
			}
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Forget Password Page</title>
</head>
<body>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<form action="" method="POST">
		<fieldset>
			<br>
			<span> <center> New Password : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="password" name="password" value="<?php echo $password;?>"> </center></span>
			<span><?php echo $passwordErrMsg ?> </span>
			<br>
			<span><center> Confirm Password : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="password" name="confirmpassword" value="<?php echo $confirmpassword; ?>"> </center></span>
			<span><?php echo $confirmpasswordErrMsg ?> </span>
			<br>
		</fieldset>
	<br>
	<center> <input type="submit" name="submit" value="Update"></center>
	</form>
	<br>
	<center> <a href="../View/NGO_Login_Page.php"> GO TO HOME</a> </center>
</body>
</html>