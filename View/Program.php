<?php

	session_start();

	$_SESSION['username'];
				
	$programname = "";
	$programtype = "";
	$motives = "";
	$description = "";
	$requiredamount = "";
	$requiredvolunteer = "";
	$bankaccountnumber = "";
	$bkashnumber = "";
	$startdate = "";
	$starttime = "";
	$closeddate = "";
	$closedtime = "";

	$programtypeErrMsg = "";
	$programnameErrMsg = "";
	$bankaccountnumberErrMsg = "";
	$bkashnumberErrMsg = "";
	$requiredamountErrMsg = "";
	$requiredvolunteerErrMsg = "";
	$startdateErrMsg = "";
	$starttimeErrMsg = "";
	$closeddateErrMsg = "";
	$closedtimeErrMsg = "";
	$motivesErrMsg = "";
	$descriptionErrMsg = "";

		if ($_SERVER['REQUEST_METHOD'] === "POST") {

			if(empty($_POST['programname'])){
				$programnameErrMsg = "<b style='color:Red;'>*Program Name is required.</b>";
			}
			else if (!preg_match ("/^[a-z A-z ]*$/", $_POST['programname']) ) {
				$programnameErrMsg = "<b style='color:Red;'>Program name allowed only alphabets and whitespace.</b>";
			}
			else{
				$programname = $_POST['programname'];
			}
			
			if(empty($_POST['programtype'])){
				$programtypeErrMsg = "<b style='color:Red;'>*Program Type is required.</b>";
			}
			else if (!preg_match ("/^[a-z A-z ]*$/", $_POST['programtype']) ) {  
				$programtypeErrMsg = "<b style='color:Red;'>Program type allowed only alphabets and whitespace.</b>";
			}
			else{
				$programtype = $_POST['programtype'];
			}

			if(strlen($_POST['bankaccountnumber']<8) && strlen($_POST['bankaccountnumber']>12)){
				$bankaccountnumberErrMsg = "<b style='color:Red;'>Bank Account Number Should Be Contain 8 to 12 number.</b>";
			}
			else{
				$bankaccountnumber = $_POST['bankaccountnumber'];
			}

			if(empty($_POST['bkashnumber'])){
				$bkashnumberErrMsg = "<b style='color:Red;'>*Bkash Number is required.</b>";
			}
			if(strlen($_POST['bkashnumber']<11) && strlen($_POST['bkashnumber']>11)){
				$bkashnumberErrMsg = "<b style='color:Red;'>Bkash Number Should Be Contain 11 number.</b>";
			}
			else{
				$bkashnumber = $_POST['bkashnumber'];
			}

			if(empty($_POST['requiredamount'])){
				$requiredamountErrMsg = "<b style='color:Red;'>*Amount is required.</b>";
			}
			else{
				$requiredamount = $_POST['requiredamount'];
			}

			if(empty($_POST['requiredvolunteer'])){
				$requiredvolunteerErrMsg = "<b style='color:Red;'>*Volunteer is required.</b>";
			}
			else{
				$requiredvolunteer = $_POST['requiredvolunteer'];
			}

			if(empty($_POST['startdate'])){
				$startdateErrMsg = "<b style='color:Red;'>*Start Date is required.</b>";
			}
			else{
				$startdate = $_POST['startdate'];
			}

			if(empty($_POST['starttime'])){
				$starttimeErrMsg = "<b style='color:Red;'>*Start Time is required.</b>";
			}
			else{
				$starttime = $_POST['starttime'];
			}

			if(empty($_POST['closeddate'])){
				$closeddateErrMsg = "<b style='color:Red;'>*Closed Date is required.</b>";
			}
			else{
				$closeddate = $_POST['closeddate'];
			}

			if(empty($_POST['closedtime'])){
				$closedtimeErrMsg = "<b style='color:Red;'>*Closed Time is required.</b>";
			}
			else{
				$closedtime = $_POST['closedtime'];
			}

			if(empty($_POST['motives'])){
				$motivesErrMsg = "<b style='color:Red;'>*Program Motives is required.</b>";
			}
			else{
				$motives = $_POST['motives'];
			}
			
			if(empty($_POST['description'])){
				$descriptionErrMsg = "<b style='color:Red;'>*Program Description is required.</b>";
			}
			else{
				$description = $_POST['description'];
			}
			
			 if(empty($bkashnumber) || empty($requiredvolunteer) ||
			 		empty($requiredamount) || empty($description) || empty($motives) || empty($programtype) || empty($programname)
			 		||empty($startdate) || empty($starttime) || empty($closeddate) || empty($closedtime)){
			 	echo " <b style='color:Red;'>Please Fill ALl Filed.</b>";
			 }
			else{
				$hostname = "localhost";
				$username = "root";
				$password = "";
				$database = "ngo_database";

				$conn = new mysqli($hostname, $username, $password, $database);

				if ($conn->connect_error) {
					die("Failed to Connect: " . $conn->connect_error);
				}

				$sql1 = "SELECT NGO_Name FROM ngo_data WHERE User_Name = ?";
				$stmt1 = $conn->prepare($sql1);
				$stmt1->bind_param('s', $User_Name);
				$User_Name = $_SESSION['username'];

				$stmt1->execute();
				$result = $stmt1->get_result();

				if ($result->num_rows > 0){

					while($row = $result->fetch_assoc()){
						$ngoname = $row["NGO_Name"];
					}
				}

				if($bkashnumberErrMsg ==='' && $bankaccountnumberErrMsg ==='' && $programnameErrMsg ==='' && $programtypeErrMsg ===''){
					$pstatus = "pending";
					$randid = rand();
					$post = "";

					$sql = "INSERT INTO start_program_data (program_id, User_name, NGO_Name, Program_Name, Program_Type, Required_Amount, Required_Volunteer, Start_Date, Closed_Date, Start_Time, Closed_Time, Bkash_Number, Bank_Account_Number, Program_Motives, Program_Description, Status) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

					$stmt = $conn->prepare($sql);
					$stmt->bind_param('issssdssssssssss', $program_id, $User_name, $NGO_Name, $Program_Name, $Program_Type, $Required_Amount, $Required_Volunteer, $Start_Date, $Closed_Date, $Start_Time, $Closed_Time, $Bkash_Number, $Bank_Account_Number, $Program_Motives, $Program_Description, $Status);

					$program_id = $randid;
					$User_name = $_SESSION['username'];
					$NGO_Name = $ngoname;
					$Program_Name = $programname;
					$Program_Type = $programtype;
					$Required_Amount = $requiredamount;
					$Required_Volunteer = $requiredvolunteer;
					$Start_Date = $startdate;
					$Closed_Date = $closeddate;
					$Start_Time = $starttime;
					$Closed_Time = $closedtime;
					$Bkash_Number = $bkashnumber;
					$Bank_Account_Number = $bankaccountnumber;
					$Program_Motives = $motives;
					$Program_Description = $description;
					$Status = $pstatus;

					if ($stmt->execute()) {
						$success = true;
					}
					else {
						echo "Something is wrong.Try again";
					}
					$conn->close();
		
						if($success){
							echo "<br>";
							echo "Congratulations Your Program Was Started";
							$_SESSION['random'] = $randid;
							echo "<br>";
							echo "<br>";
							echo "<hr>";
							echo "<br>";
							echo "<center>Please wait for admin to accepted your program. See more details go to".' <a href="../View/Pending_Program.php">pending program</a> option</center>';
						}
						else{
							echo "Something Is Missing. Try TO Start A New Program Again.";
							echo "<br>";
							echo "<hr>";
						}
				}
				else{
					
				}
			}
		}
	else{
		
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Program Start Page</title>
</head>
<body>
	<br>
	<hr>
	<center> <h2>Start Program</h2></center>
	<hr>
	<?php
		require '../Controller/Menu_Header.php';
	?>
	<br>
	<form action="" method="POST">
		<fieldset>
			<legend>Program Information</legend>
			<br>
			Program Name : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="text" name="programname" placeholder="Program Name" <?php echo $programname; ?> >
			<span><?php echo $programnameErrMsg; ?></span>
			<br>
			<br>
			Program Type : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="text" name="programtype" placeholder="Program Type" <?php echo $programtype; ?>>
			<span><?php echo $programtypeErrMsg; ?> </span>
			<br><br>
			Motives : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?php echo $motives; ?>
			<span><?php echo $motivesErrMsg; ?> </span>
			<textarea id="motives" name="motives" rows="3" cols="50" >  </textarea>
			<br>
			<br>
			Description : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?php echo $description; ?>
			<span><?php echo $descriptionErrMsg; ?> </span>
			<textarea id="description" name="description" rows="3" cols="50"> </textarea>
			<br><br>
			
		</fieldset>
		<br>
		<fieldset>
			<legend>Program Required Information</legend>
			<br>
			Required Amount : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="number" name="requiredamount" placeholder="Required Amount" <?php echo $requiredamount; ?>>
			<span><?php echo $requiredamountErrMsg; ?></span>
			<br><br>
			Required Volunteer : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="number" name="requiredvolunteer" placeholder="Required Volunteer" <?php echo $requiredvolunteer; ?>>
			<span><?php echo $requiredvolunteerErrMsg; ?></span>
			<br><br>
			Program Start Date : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="date" name="startdate" placeholder="Select Date" <?php echo $startdate; ?>>
			<span><?php echo $startdateErrMsg; ?></span>
			<br><br>
			Program Start Time : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="time" name="starttime" placeholder="Start Time" <?php echo $starttime; ?>>
			<span><?php echo $starttimeErrMsg; ?></span>
			<br><br>
			Program Closed Date : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="date" name="closeddate" placeholder="Select Date" <?php echo $closeddate; ?>>
			<span><?php echo $closeddateErrMsg; ?></span>
			<br><br>
			Program Closed Time : &nbsp;&nbsp;&nbsp;&nbsp; <input type="time" name="closedtime" placeholder="Start Time" <?php echo $closedtime; ?>>
			<span><?php echo $closedtimeErrMsg; ?></span>
			<br><br>
		</fieldset>
		<br>
		<fieldset>
			<br>
			<legend>Account Information</legend>
			Bank Account Number : &nbsp;&nbsp; <input type="number" name="bankaccountnumber" placeholder="Bank Account Number" <?php echo $bankaccountnumber; ?>>
			<span><?php echo $bankaccountnumberErrMsg; ?> </span>
			<br><br>
			Bkash Number : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="number" name="bkashnumber" placeholder="Bkash Number" <?php echo $bkashnumber; ?>>
			<span><?php echo $bkashnumberErrMsg; ?> </span>
			<br><br>
		</fieldset>
		<br>
		<br>
		<center><input type="submit" name="Start" value="Start" style="width: 15%;"></center>
	</form>
	<br><br>
	<hr>
	<br>
	<?php
		
		include '../Controller/Footer.php';
		
	?>
</body>
</html>