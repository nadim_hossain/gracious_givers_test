<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Registration Action</title>
</head>
<body>
	<?php 
		if ($_SERVER['REQUEST_METHOD'] === "POST") {
						
			$ngoname = $_POST['NGOname'];
			$ngoownername = $_POST['NGOownername'];
			$password = $_POST['password'];
			$username = $_POST['username'];
			$typeofngo = $_POST['TypeofNGO'];
			$email = $_POST['email'];
			$confirmpassword = $_POST['confirmpassword'];
			$url = $_POST['url'];
			$presentaddress = $_POST['prenentaddress'];
			$permanentaddress = $_POST['permanentaddress'];
			// $bankaccountnumber = $_POST['bankaccountnumber'];
			// $motives = $_POST['motives'];
			// $description = $_POST['description'];
			$ngonameErrMsg = $ngoownernameErrMsg = $emailErrMsg = $usernameErrMsg = $passwordErrMsg = $confirmpasswordErrMsg = $ngoownernameErrMsg = $typeofngoErrMsg = '';
	
			if (!preg_match ("/^[a-z A-z ]*$/", $ngoname) ) {  
				$ngonameErrMsg = "Only alphabets and whitespace are allowed.";
			}
	
			if (!preg_match ("/^[a-z A-z ]*$/", $ngoownername) ) {  
				$ngoownernameErrMsg = "Only alphabets and whitespace are allowed.";
			}
	
			if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
				$emailErrMsg = "Invalid email format. Email like : example@example.com";
			}
	
			if (!preg_match ("/^[a-zA-z0-9]*$/", $username) ) {  
				$usernameErrMsg = "Alphabets and Numbers combination are allowed.";
			}
	
			$number = preg_match('@[0-9]@', $password);
			$uppercase = preg_match('@[A-Z]@', $password);
			$lowercase = preg_match('@[a-z]@', $password);
			$specialChars = preg_match('@[^\w]@', $password);
			
			if(strlen($password) < 8 || !$number || !$uppercase || !$lowercase || !$specialChars) {
				$passwordErrMsg = "Password must be at least 8 characters in length and must contain at least one number, one upper case letter, one lower case letter and one special character.";
			}
	
			if($password != $confirmpassword){
				$confirmpasswordErrMsg = "Enter Same Password.";
			}
	
			if(isset($_REQUEST['TypeofNGO']) && $_REQUEST['TypeofNGO'] === '0') { 
				$typeofngoErrMsg = "Select Your NGO Type.";
			}
			
			if(empty($permanentaddress) || empty($presentaddress) || empty($url) || empty($confirmpassword) || empty($email)
					|| empty($typeofngo) || empty($username) || empty($password) || empty($ngoownername) || empty($ngoname)){
						echo "<br>";
				echo "Please Fill All Filed.";
			}
			else{

				if($ngonameErrMsg==='' && $emailErrMsg==='' && $usernameErrMsg==='' && $passwordErrMsg==='' && $confirmpasswordErrMsg === '' && $ngoownernameErrMsg === '' && $typeofngoErrMsg === ''){
					$handle = fopen("../Model/NGO's_Data.json", "a");
						$arr1 = array('NGO_Name' => $ngoname, 'User_name' => $username, 'Password' => $password,
									'NGO_Owner_Name' => $ngoownername, 'Type_Of_NGO' => $typeofngo, 'Email_Address' => $email,
									 'Present_Address' => $presentaddress, 'Permanent_Address' => $permanentaddress, 'NGOs_Web_Link' => $url);
						$json = json_encode($arr1);
						$success = fwrite($handle, $json."\n");
		
						if($success){
							echo "<br>";
							echo "Registration Successfull..";
						}
						else{
							echo "<br>";
							echo "Registration Failed..";
						}
		
		
						
				}
				else{
					echo "<br>";
					echo $ngonameErrMsg;
					echo "<br>";
					echo $emailErrMsg;
					echo "<br>";
					echo $usernameErrMsg;
					echo "<br>";
					echo $passwordErrMsg;
					echo "<br>";
					echo $confirmpasswordErrMsg;
					echo "<br>";
					echo $typeofngoErrMsg;
					echo "<br>";
					echo $ngoownernameErrMsg;
					echo "<br>";
					echo '<center><a href = "../View/NGO_Registrasion_Page.php"> Continue Registration </a></center>';
			}
		}
	}
	else{
		echo "Request Server Failed";
	}
?>
	<br>
	<br>
	<hr>
	<br>
		<center> <h1>  WElCOME TO NGO'S OPTION</h1> </center>
		<center> <h2>What Do You Want to do ?</h2> </center>
		<center> <a href = "../View/NGO_Login_Page.php"> Login </a> </center>
		<br>
		<center> <a href = "../View/NGO_Registrasion_Page.php"> Registration </a> </center>
		<br><br><br><br><br>
		<hr>
		<br>
		<?php
			include '../Controller/Footer.php';
		?>
</body>
</html>